//
//--------------------------------------------------------------------------------------------------
//Problem url: https://codeforces.com/contest/339/problem/A
//--------------------------------------------------------------------------------------------------
//
//
//  █████████████████████████████████████████████████████████████████████████████████████████
//  ███▄─▄████─▄▄─██─▄▄▄▄─█─███─███ ▄▄▄─██ ███ ██ ▄▄▄ █ ▄▄▄─█ ▄▄▄─█▄─▄████─▄▄─██─▄─▄─█ ▄▄▄███
//  ████─█████────█████──███─█─████ ██████ ─── ██ ███ █ █████ ███ ██─█████────████ ███ ▄▄▄███
//  ████─██▀██─██─███──██████─█████ ██████ ███ ██ ███ █ █████ ███ ██─██▀██─██─████ ███ ██████
//  ███─ ▄▄▄█─ ██ ─█──▄▄▄▄██───████▄▄▄▄─██─ ██ ─█▄▄▄▄▄█▄▄▄▄─█▄▄▄▄▄█─ ▄▄▄█─ ██ ─██─ ─██▄▄▄─███
//  █████████████████████████████████████████████████████████████████████████████████████████

#include <bits/stdc++.h>
#define nl '\n'
#define PB push_back
#define MP make_pair
#define strsync ios::sync_with_stdio(0);cin.tie(0)
using namespace std;

int main() {
    strsync;
    
    int n,a{0},b{0},c{0},s{0};
    char k;
    while(cin>>n){
        switch (n){
            case 1:++a; break;
            case 2:++b; break;
            case 3:++c; break;
        }
        ++s;
        cin>>k;
    }
    while(a--){
        cout<<1;
        s--;
        if(s)   cout<<'+';
    }
    while(b--){
        cout<<2;
        s--;
        if(s)   cout<<'+';
    }
    while(c--){
        cout<<3;
        s--;
        if(s)   cout<<'+';
    }
}

