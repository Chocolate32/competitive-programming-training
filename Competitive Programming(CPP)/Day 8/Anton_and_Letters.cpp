//
//--------------------------------------------------------------------------------------------------
//Problem url: https://codeforces.com/contest/443/problem/A
//--------------------------------------------------------------------------------------------------
//
//
//  █████████████████████████████████████████████████████████████████████████████████████████
//  ███▄─▄████─▄▄─██─▄▄▄▄─█─███─███ ▄▄▄─██ ███ ██ ▄▄▄ █ ▄▄▄─█ ▄▄▄─█▄─▄████─▄▄─██─▄─▄─█ ▄▄▄███
//  ████─█████────█████──███─█─████ ██████ ─── ██ ███ █ █████ ███ ██─█████────████ ███ ▄▄▄███
//  ████─██▀██─██─███──██████─█████ ██████ ███ ██ ███ █ █████ ███ ██─██▀██─██─████ ███ ██████
//  ███─ ▄▄▄█─ ██ ─█──▄▄▄▄██───████▄▄▄▄─██─ ██ ─█▄▄▄▄▄█▄▄▄▄─█▄▄▄▄▄█─ ▄▄▄█─ ██ ─██─ ─██▄▄▄─███
//  █████████████████████████████████████████████████████████████████████████████████████████

#include <bits/stdc++.h>
#define nl '\n'
#define PB push_back
#define MP make_pair
#define strsync ios::sync_with_stdio(0);cin.tie(0)
using namespace std;

int main() {
    strsync;
    
    unordered_set<char> a;
    char c;
    while(cin>>c){
        if(c!='{'&&c!='}'&&c!=',')  a.insert(c);
    }
    cout<<a.size();
}

