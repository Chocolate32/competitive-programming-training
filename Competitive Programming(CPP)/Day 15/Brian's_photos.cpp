//
//--------------------------------------------------------------------------------------------------
//Problem url: https://codeforces.com/contest/707/problem/A
//--------------------------------------------------------------------------------------------------
//
//
//  █████████████████████████████████████████████████████████████████████████████████████████
//  ███▄─▄████─▄▄─██─▄▄▄▄─█─███─███ ▄▄▄─██ ███ ██ ▄▄▄ █ ▄▄▄─█ ▄▄▄─█▄─▄████─▄▄─██─▄─▄─█ ▄▄▄███
//  ████─█████────█████──███─█─████ ██████ ─── ██ ███ █ █████ ███ ██─█████────████ ███ ▄▄▄███
//  ████─██▀██─██─███──██████─█████ ██████ ███ ██ ███ █ █████ ███ ██─██▀██─██─████ ███ ██████
//  ███─ ▄▄▄█─ ██ ─█──▄▄▄▄██───████▄▄▄▄─██─ ██ ─█▄▄▄▄▄█▄▄▄▄─█▄▄▄▄▄█─ ▄▄▄█─ ██ ─██─ ─██▄▄▄─███
//  █████████████████████████████████████████████████████████████████████████████████████████

#include <iostream>
#define nl '\n'
#define PB push_back
#define MP make_pair
#define strsync ios::sync_with_stdio(0);cin.tie(0)
using namespace std;

int main() {
    strsync;
    char c;
    int n,m;
    bool colored =0;
    cin>>n>>m;
    n*=m;
    while (n--) {
        cin>>c;
        colored+=((c=='C')||(c=='M')||(c=='Y'));
    }
    
    cout<<(colored?"#Color":"#Black&White");
    
    return 0;
}

