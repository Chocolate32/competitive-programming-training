//
//--------------------------------------------------------------------------------------------------
//Problem url: https://codeforces.com/contest/731/problem/A
//--------------------------------------------------------------------------------------------------
//
//
//  █████████████████████████████████████████████████████████████████████████████████████████
//  ███▄─▄████─▄▄─██─▄▄▄▄─█─███─███ ▄▄▄─██ ███ ██ ▄▄▄ █ ▄▄▄─█ ▄▄▄─█▄─▄████─▄▄─██─▄─▄─█ ▄▄▄███
//  ████─█████────█████──███─█─████ ██████ ─── ██ ███ █ █████ ███ ██─█████────████ ███ ▄▄▄███
//  ████─██▀██─██─███──██████─█████ ██████ ███ ██ ███ █ █████ ███ ██─██▀██─██─████ ███ ██████
//  ███─ ▄▄▄█─ ██ ─█──▄▄▄▄██───████▄▄▄▄─██─ ██ ─█▄▄▄▄▄█▄▄▄▄─█▄▄▄▄▄█─ ▄▄▄█─ ██ ─██─ ─██▄▄▄─███
//  █████████████████████████████████████████████████████████████████████████████████████████

#include <bits/stdc++.h>
#define nl '\n'
#define PB push_back
#define MP make_pair
#define strsync ios::sync_with_stdio(0);cin.tie(0)
using namespace std;


int main() {
    strsync;
    
    
    int s{0},l,r,pos;
    
    char c,temp,a[78],p{'a'};
    for(char i{'a'};i<='z';++i){
        a[i-97]=i;
        a[i-97+26]=i;
        a[i-97+52]=i;
    }
    
    
    while(cin>>c){
        l=0;
        r=0;
        
        if(p==c) continue;
        
        temp=p;
        
        pos=p-97+26;
        
        while(p!=c) {
            p=a[pos + (++r)];
        }
        
        pos=temp-97+26;
        while(temp!=c) {
            temp=a[pos - (++l)];
        }
        
        p=c;
        
        s+=r<l?r:l;
    }
    
    cout<<s;
    
}
