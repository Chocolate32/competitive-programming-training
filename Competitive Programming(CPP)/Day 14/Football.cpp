
//
//--------------------------------------------------------------------------------------------------
//Problem url: https://codeforces.com/contest/43/problem/A
//--------------------------------------------------------------------------------------------------
//
//
//  █████████████████████████████████████████████████████████████████████████████████████████
//  ███▄─▄████─▄▄─██─▄▄▄▄─█─███─███ ▄▄▄─██ ███ ██ ▄▄▄ █ ▄▄▄─█ ▄▄▄─█▄─▄████─▄▄─██─▄─▄─█ ▄▄▄███
//  ████─█████────█████──███─█─████ ██████ ─── ██ ███ █ █████ ███ ██─█████────████ ███ ▄▄▄███
//  ████─██▀██─██─███──██████─█████ ██████ ███ ██ ███ █ █████ ███ ██─██▀██─██─████ ███ ██████
//  ███─ ▄▄▄█─ ██ ─█──▄▄▄▄██───████▄▄▄▄─██─ ██ ─█▄▄▄▄▄█▄▄▄▄─█▄▄▄▄▄█─ ▄▄▄█─ ██ ─██─ ─██▄▄▄─███
//  █████████████████████████████████████████████████████████████████████████████████████████

#include <iostream>
#define nl '\n'
#define PB push_back
#define MP make_pair
#define strsync ios::sync_with_stdio(0);cin.tie(0)
using namespace std;

int main() {
    strsync;
    
   int t;
   string s;
   pair<string,int> a,b;
   cin>>t;
    cin>>s;
    a=MP(s,1);
    b=MP(s,0);
    t--;
   while(t--){
       cin>>s;
       if(s==a.first){
           a.second++;
       }
       else{
           if(!b.second)    b.first=s;
           b.second++;
       }
   }
   cout<<(a.second>b.second?a.first:b.first);
    
    return 0;
}

